export class SignalViewModel {
    employeeName: string | undefined;
    employeeId: string  | undefined;
    description: string | undefined;
    signalStamp: string | undefined;
  }