**Asp.Net Core 3.1 SignalR and Angular 11.0.5**


**CURL**
```
curl --location --request POST 'http://localhost:62827/api/v1/signals/deliverypoint' \
--header 'Content-Type: application/json' \
--data-raw '{
    "employeeId": "12345",
    "employeeName": "Tazbir",
    "description": "Test Signal"
}'
```
