Create Database SignalRDB
go
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](100) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[EmployeeId] [varchar](20) NOT NULL,
	[SignalDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (1, N'Mazhar', N'Test Signal', N'12345', CAST(N'2020-12-30T16:16:34.803' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (2, N'Mazhar', N'Test Signal', N'12345', CAST(N'2020-12-30T16:16:50.357' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (3, N'Mazhar', N'Test Signal', N'12345', CAST(N'2020-12-30T17:19:10.643' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (4, N'Mazhar', N'Test Signal', N'12345', CAST(N'2020-12-30T17:21:19.637' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (5, N'Mazhar', N'Tazbir Bhai', N'12345', CAST(N'2020-12-30T17:26:07.193' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (6, N'Mazhar2', N'hh', N'12345', CAST(N'2020-12-30T17:26:18.033' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (7, N'Mazhar2', N'kk', N'12345', CAST(N'2020-12-30T17:31:49.073' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (8, N'Mazhar11ss11', N'eeee', N'12345', CAST(N'2020-12-30T17:32:15.480' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (9, N'Mazhar1888111', N'sss', N'12345', CAST(N'2020-12-30T17:32:31.683' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (10, N'Mazhar22ff2222', N'ttt', N'12345', CAST(N'2020-12-30T17:32:33.357' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (11, N'hh', N'goo', N'12345', CAST(N'2020-12-30T18:30:00.493' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (12, N'Mazhar1dd1ss1', N'I''m done 2', N'12345', CAST(N'2020-12-30T18:33:05.787' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (13, N'Mazhar1dsfsfd2', N'i''m done', N'12345', CAST(N'2020-12-30T18:36:19.337' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (14, N'Mazhar1', N'Test Signal', N'12345', CAST(N'2020-12-30T20:42:21.493' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (15, N'Mazhar1', N'Test Signal', N'12345', CAST(N'2020-12-30T20:55:44.503' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (16, N'Mazhar1', N'Test Signal', N'12345', CAST(N'2020-12-30T21:33:30.327' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (17, N'Mazhar1', N'Test Signal', N'12345', CAST(N'2020-12-30T23:33:05.670' AS DateTime))
GO
INSERT [dbo].[Employees] ([Id], [EmployeeName], [Description], [EmployeeId], [SignalDate]) VALUES (18, N'Tazbir', N'Test Signal', N'12345', CAST(N'2020-12-31T15:19:29.580' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Employees] OFF
GO


--To enable Service Broker run:
ALTER DATABASE [Database_name] SET ENABLE_BROKER;
go

--To check if Service Broker is enabled on a SQL Server database:
SELECT is_broker_enabled FROM sys.databases WHERE name = 'SignalRDB';

