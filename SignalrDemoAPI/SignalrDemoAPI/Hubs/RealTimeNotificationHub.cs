﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Configuration;
using SignalrDemoAPI.Models;

namespace SignalrDemoAPI.Hubs
{
    public class RealTimeNotificationHub : Hub
    {
        private SqlDependency dependency;
        private string connectionString;
        protected IHubContext<RealTimeNotificationHub> _hubContext;

        public RealTimeNotificationHub(IHubContext<RealTimeNotificationHub> context)
        {
            this._hubContext = context;
        }


        public override async Task OnConnectedAsync()
        {
            connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionString").Value;
            SqlDependency.Start(connectionString);
            SqlDependency.Stop(connectionString);

            await base.OnConnectedAsync();
        }




        //[HubMethodName("SendRealTimeNotifications")]
        public async Task SendRealTimeNotifications()
        {
            DataTable dt = new DataTable();
            connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionString").Value;

  
            SqlDependency.Start(connectionString);
            SqlConnection connection = new SqlConnection(connectionString);
            //connection.Open();

            try
            {
                SqlCommand command = new SqlCommand();

                string commandText = "SELECT [EmployeeName],[Description],[EmployeeId],[SignalDate] FROM [dbo].[Employees]";
                /*command.CommandText = commandText;
                command.Connection = connection;
                command.CommandType = CommandType.Text;

                dependency = new SqlDependency(command);
                dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);


               dependency.AddCommandDependency(command);
               var reader = command.ExecuteReader();*/

               


                using (var conn = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(commandText, connection))
                    {
                        command.Notification = null;
                        dependency = new SqlDependency(cmd);
                        dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                        var dataReader = cmd.ExecuteReader();
                        dt.Load(dataReader);
                        connection.Close();
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            var res = from DataRow employee in dt.Rows
                        select new SignalViewModel()
                        {
                            EmployeeId = employee["EmployeeId"].ToString(),
                            EmployeeName = employee["EmployeeName"].ToString(),
                            Description = employee["Description"].ToString(),
                            SignalStamp = employee["SignalDate"].ToString()

                        };

            await this._hubContext.Clients.All.SendAsync("RecieveNotification", res.ToList());
        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
             if (e.Type == SqlNotificationType.Change && (e.Info == SqlNotificationInfo.Insert ||
                 e.Info == SqlNotificationInfo.Delete ||
                 e.Info == SqlNotificationInfo.Update))
            {
                RealTimeNotificationHub realTimeNotificationHub = new RealTimeNotificationHub(this._hubContext);
                realTimeNotificationHub.SendRealTimeNotifications();
            }
        }
    }
}
